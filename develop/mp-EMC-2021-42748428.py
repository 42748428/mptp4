# EMC: 2021
# Nombre del Alumno: Daniel Ernesto Ajalla
# DNI: 42748428

import os


def menu(): 
    os.system('cls')
    print('1) Agregar empleado.')
    print('2) Mostrar los empleados.')
    print('3) Cambiar departamento.')
    print('4) Ordenar empleado')
    print('5) Salir.')
    while True:
        try:
            opc =  int(input('Ingrese una opcion: '))
            if (opc>=1) and (opc<=5):
               break
            else:
                print('Opcion no identificado!.. intente de nuevo...')
        except ValueError:
            print('Ingrese opcion correctamente...')         
    return  opc
def agregar(empleados):
    ide=id(empleados)
    nombres=nombre()  
    correos=correo(empleados)
    departamentos=departamento()
    salarios=salario()
    atributos=[ide,nombres,correos,departamentos,salarios]
    empleados.insert(0,atributos)
    continuar()
def continuar():
    input("Enter para continuar")
        
def id(empleados):
    while True:
        id=int(input("Ingrese un identificacion: "))
        if id in empleados[0]:
            print("existente!!!")
        elif id=='':
            print("vuelva a ingresar")
        else:
            return id
def nombre():
    while True:
        nombre=input("Ingrese un nombre:")
        if nombre!='':
            return nombre
def correo(empleados):    # MODELO PARA VALIDAR CORREO
    validado=True
    while validado:
        correo=input("Ingrese un correo personal:")
        if correo!='':
            for datos in empleados:
                if correo==datos[2]:
                    validado=True
                    print("ya existe!")
                    break
                else:
                    dato=list(correo)
                    if(dato.count('@')==1): 
                        validado=False 
                    else:
                        print("no contiene =>@<=")
                        validado=True 
                        break       
        else:
            print("debe ingresar un correo")
            validado=True
    return correo          
            
def departamento():
    while True:
        departamento=int(input("seleccione el departamento ( 1)Gerencia; 2)Venta; 3)Compras ): "))  
        if departamento==1:
            depar='Gerencia'
            return depar
        elif departamento==2:
            depar='Ventas'  
            return depar  
        elif departamento==3:
            depar='Compras' 
            return depar
def salario():
    while True:
        salario=int(input("Ingrese un salario: "))
        if salario!='':
            return salario
def mostrar(empleados):
    id=int(input("Ingrese una identificacion: "))
    for i in range(len(empleados)):
        if id<=empleados[i][0]:
            print(empleados[i])
    continuar()
def cambiar(empleados):
    departament=int(input("seleccione el departamento ( 1)Gerencia; 2)Venta; 3)Compras ): "))  
    nom=input("ingrese nombre: ")
    if departament==1:
        depar='Gerencia'        
    elif departament==2:
        depar='Ventas'        
    elif departament==3:
        depar='Compras' 
    for i in range(len(empleados)):
        if nom==empleados[i][1]:
            empleados[i][3]=depar
            return empleados
def ordenar(empleados): # MODELO DE ORDENAMIENTO CON LISTA pos loguitud de palabra
    n=len(empleados)
    salir=True
    while salir:
        salir=False
        for i in range(n-1):
            x=len(list(empleados[i][1]))
            x1=len(list(empleados[i+1][1]))
            if x<x1:
                auxiliar=empleados[i]
                empleados[i]=empleados[i+1]
                empleados[i+1]=auxiliar
                salir=True
    print(empleados)
    continuar()


   
empleados = [[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Comrpas", 3100]
            ]
# A continuación un ejemplo que muestra la lista. Puede borrarlo            
opc=0

while opc!=5: 
    opc=menu()
    if opc==1:      
        agregar(empleados)          
    elif opc==2:
        mostrar(empleados) 
    elif opc==3:
        empleados=cambiar(empleados)   
        continuar()
    elif opc==4:
        ordenar(empleados)
    elif opc==5:
        print('FIN DEL PROGRAMA')