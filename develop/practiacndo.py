def bubbleSort(lst):
 n = len(lst)
 for i in range(n):
    for j in range(0, n-1):
        if lst[j] > lst[j+1]:
            lst[j], lst[j+1] = lst[j+1], lst[j]
 return lst
lis=[1,1,2,3,5,6,43,2,24,3,4] 

print(bubbleSort(lis))
def insertionSort(lst):
 for step in range(1, len(lst)):
    key = lst[step]
    j = step - 1
    while j >= 0 and key < lst[j]:
        lst[j + 1] = lst[j]
        j = j - 1
        lst[j + 1] = key
 return lst
print(insertionSort(lis))
def selectionSort(lst):
 n = len(lst)
 for step in range(n):
    j = step
    for i in range(step + 1, n):
       if lst[i] < lst[j]:
        j = i
        lst[step], lst[j] = lst[j], lst[step]
 return lst
print(selectionSort(lis))
def shellSort(lst):
 n = len(lst)
 gap = n // 2
 while gap > 0:
    for i in range(gap, n):
        tmp = lst[i]
        j = i
        while j >= gap and lst[j - gap] > tmp:
            lst[j] = lst[j - gap]
            j -= gap
            lst[j] = tmp
            gap = gap // 2
 return lst