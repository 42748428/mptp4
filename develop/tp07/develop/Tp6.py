
import os
from typing import Final


def menu(): 
    os.system('cls')
    print('1) Leer un archivo y guardar.')
    print('2) Buscar vehiculo por el Dominio.')
    print('3) Buscar vehiculo por la Marca.')
    print('4) Crear un fichero de una misma Marca determinado de un vehiculo.')
    print('5) Salir.')
    while True:
        try:
            opc =  int(input('Ingrese una opcion: '))
            if (opc>=1) and (opc<=5):
               break
            else:
                print('Opcion no identificado!.. intente de nuevo...')
        except ValueError:
            print('Ingrese opcion correctamente...')         
    return  opc
def lyg():
    print('EL ARCHIVO DEBE DE ESTAR EN LA MISMA CARPETA!..')
    r='C:\\Users\\ernes\\OneDrive\\Documentos\\MP2021\\mptp04\\develop\\'
    n=input('ingrese nombre del archivo: ').lower()
    z=n +'.txt'
    papeles=open(r+z, 'r') 
    personal=[] 
    atrib=papeles.readlines()
    for i in atrib:            
        atrib=i.split(';') 
        print(atrib) 
        print('.......................CARGANDO...........................')
        archivo={}   
        linea=atrib
        dominio=linea[0]
        marca=linea[1]
        tipo=linea[2]
        modelo=linea[3]
        kilometrje=linea[4]
        precio=linea[5]
        preciov=linea[6]
        estado=linea[7]
        archivo['dominio']=dominio
        archivo['marca']=marca
        archivo['tipo']=tipo
        archivo['modelo']=modelo
        archivo['kilometraje']=kilometrje
        archivo['precio']=precio
        archivo['preciov']=preciov
        archivo['estado']=estado
        personal.append(archivo)
    papeles.close()                      
    return personal
                                   
def buscar(cliente):
    lista=[]
    for i in range(len(cliente)):
        domi=cliente[i]['dominio']  
        lista.append(domi)         
    pos=main(lista)    
    for j in range(len(lista)):
        if pos==lista[j]:        
            print('--------------------------------------------')
            print("Dominio: ", cliente[j]['dominio']) 
            print("Marca: ", cliente[j]['marca'])  
            print("Tipo: ", cliente[j]['tipo'])  
            print("Modelo: ", cliente[j]['modelo'])  
            print("Kilometraje: ", cliente[j]['kilometraje'])  
            print("Precio evaluado: $", cliente[j]['precio'])  
            print("Precio para la venta: $", cliente[j]['preciov'])  
            print("Estado: ", cliente[j]['estado']) 
            print('---------------------------------------------')
            break 
    input('Precione enter para volver al menu...')         
def buscarmarca(cliente):
    lista=[]
    for i in range(len(cliente)):
        domi=cliente[i]['marca']  
        lista.append(domi)        
    pos=main(lista)    
    for j in range(len(lista)):
        while pos==lista[j]:        
            print('--------------------------------------------')
            print("Dominio: ", cliente[j]['dominio']) 
            print("Marca: ", cliente[j]['marca'])  
            print("Tipo: ", cliente[j]['tipo'])  
            print("Modelo: ", cliente[j]['modelo'])  
            print("Kilometraje: ", cliente[j]['kilometraje'])  
            print("Precio evaluado: $", cliente[j]['precio'])  
            print("Precio para la venta: $", cliente[j]['preciov'])  
            print("Estado: ", cliente[j]['estado']) 
            print('---------------------------------------------')
            break
    input('Precione enter para volver al menu...')
def crear(cliente):
    """creando archivo"""
    lista=[]
    for i in range(len(cliente)):
        domi=cliente[i]['marca']  
        lista.append(domi)        
    while True:
        pos=main(lista) 
        n=(pos)
        try: 
            n=(n+".txt")
            archivo=open(n, 'x', encoding='utf-8')  
            for j in range(len(lista)):
                while pos==lista[j]:
                    valor0='--------------------------------------------'
                    valor1='Dominio: ' + cliente[j]['dominio']
                    valor2='Marca: ' + cliente[j]['marca'] 
                    valor3='Tipo: ' + cliente[j]['tipo']  
                    valor4='Modelo: ' + cliente[j]['modelo']
                    valor5='Kilometraje: ' + cliente[j]['kilometraje']  
                    valor6='Precio evaluado: ' + cliente[j]['precio'] 
                    valor7='Precio de venta: ' + cliente[j]['preciov'] 
                    valor8='Estado: ' + cliente[j]['estado'] 
                    archivo.write(valor1 + '\n')
                    archivo.write(valor2 + '\n')
                    archivo.write(valor3 + '\n')
                    archivo.write(valor4 + '\n')
                    archivo.write(valor5 + '\n')
                    archivo.write(valor6 + '\n')
                    archivo.write(valor7 + '\n')
                    archivo.write(valor8 + '\n')
                    archivo.write(valor0 + '\n')
                    
                    break
            print('-----------------archivo creado------------------')    
            archivo.close()   
            break 
        except FileExistsError or TypeError:
            print('Esta archivo ya existe o se produjo un error, ingrese correctamente...')
        print('..................................................')
    input('Preciona enter para continuar...')
def busquedacaracter(lista, x):
    '''Busca registro por nombre. Búsqueda lineal'''
    encontrados=[]
    for registro in lista:
        if registro==x:
            encontrados.append(registro)
    if len(encontrados)==0:
        return None
    else:
        return encontrados            
  


def main(lista):

    x = input("¡Ingrese Marca para buscar!: ").title() 
   
    resultado = busquedacaracter(lista, x) 
    if resultado==None:
        print('No existe registro con este dato')
    else:
        for resultado in resultado:
            return resultado



opc=0
cliente=[]
while opc!=5: 
    opc=int(menu())
    if opc==1:      
        cliente=lyg() 
        input('Precione enter para continuar...')       
    elif opc==2:
        buscar(cliente) 
    elif opc==3:
        buscarmarca(cliente)
    elif opc==4:
        crear(cliente)
    elif opc==5:
        print('FIN DEL PROGRAMA')
            


 
