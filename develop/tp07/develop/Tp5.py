
import os
from operator import attrgetter, itemgetter


def menu(): 
    os.system('cls')
    print('1) Agregar vehiculos.')
    print('2) Reservar un automovil(R).')
    print('3) Buscar automovil por dominio.')
    print('4) Ordenar por marca de forma ascendente o descendente.')
    print('5) Ordenar por precio de venta de forma ascendente o descendente.')
    print('6) Salir.')
    while True:
        try:
            opc =  int(input('Ingrese una opcion: '))
            if (opc>=1) and (opc<=6):
               break
            else:
                print('Opcion no identificado!.. intente de nuevo...')
        except ValueError:
            print('Ingrese opcion correctamente...')         
    return  opc

def Agregar():
    lista=[]   
    archivo={}
    while True:
        try:         
            num= int(input('¿Cual es la cantidad de vehiculos para guardar en el archivo? : '))
            if num>0:
                break
            else:
                print('Ingrese numero positivo')
        except ValueError:
            print("Ingrese numero")        
    print('En esta agencia solo trabajamos con tres marcar (Renault, Ford, Citroen)')
    diccionario={'mar':['Renault','Ford', 'Citroen'], 'tipo':['Utilitario', 'Automovil'], 'modelo':[2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020], 'estado':'Disponible'}
    
    print('AHORA INGRESE LOS ATRIBUTOS DEL AUTO SOLICITADO A CONTINUACION!..')
    print('....................................................')  
    for i in range(0,num):
        Dominio={}
        print('...............AGREGAR VEHICULO Nº ',(i+1)  ,'.........................') 
        Dominio1=validardom()
        Dominio['Dominio1']=Dominio1
        print('........................................')
        Marca=evaluarmarca(diccionario) 
        Dominio['Marca']=Marca 
                 
        print('........................................')     
        while True:
            Tipo=input('tipo de vehiculo (U/A): ').title()
            if Tipo in diccionario['tipo']:
                Dominio['Tipo']=Tipo

                break
            else:
                print('Tipo de vehiculo no reconocido! intente de nuevo...')
        print('........................................')
        while True:
            Modelo=int(input('Modelo del vehiculo[2005,2020]: '))
            if Modelo>=diccionario['modelo'][0] and Modelo<=diccionario['modelo'][15]:
                Dominio['Modelo']=Modelo
                
                break
            else:
                print('Modelo invalido! intente de nuevo...')
        print('........................................')
        while True:
            try:
                Kilometraje=float(input('Kilometraje del vehiculo: '))
                if Kilometraje>=0:
                    Dominio['Kilometraje']=Kilometraje
                    
                    break
            except ValueError:
                print('Ingrese el Kilometraje correctamente...')
        print('........................................')                     
        while True:
            eval=0
            i=0
            try:
                i=int(input('Ingrese su tipo del vehiculo nuevamente :::: 1) Utilitario o 2) Automovil: '))
                i-=1
                if i==0 or i==1:
                    eval=evaluarpre(Dominio['Modelo'], diccionario['modelo'])
                    Dominio['eval']=eval               
                    break                   
                elif i!=1 and i!=0:
                    print('Tipo de auto no identificado')
            except ValueError:
                print("Debe ingresar un numero")        
        print('.......................................')
        print('El precio de venta se aumenta el (10 %)')                  
        preciov=0   
        evalu=0
        evalu=float(Dominio['eval']/10)
        preciov=float(Dominio['eval']+evalu)
        Dominio['preciov']=preciov     
        print('Precio para la venta es: ',Dominio['preciov'])
        print('........................................')
        D=diccionario['estado']
        Dominio['Estado']=D 
        print('........Se guardo los atributos.......')
        archivo[Dominio1]= Dominio         
    lista.append(archivo)     
    return lista    

def evaluarmarca(mar):         
    while True:
        try:
            marca=input('Marca del vehiculo (R/F/C): ').title()             
            if marca in mar['mar']:
                break
        except ValueError:
            print('Modelo no indentificado, intente de nuevo!..')
    return marca
            
def evaluarpre(Modelo, modelo):
    while True:
        try:
            Preciovaluado=float(input('Precio evaluado del vehiculo: '))
            if Modelo in modelo:
                if Preciovaluado>0: 
                    break
            else:
                print('El precio no es aceptable! intente nuevo...') 
        except ValueError:
            print('DEBE SER UN NUMERO')        
    return Preciovaluado          

def Reservar(lista):  
    R='Reservado'
    print('Puedes reservar un vehiculo de acuerdo a la marca')  
    while True:
        mod=input('ingrese el marca del vehiculo: ').title()
        for i in range(len(lista)):
            for key in sorted(lista[i], key=str.lower):
                if lista[i][key]['Marca']==mod:
                    lista[i][key]['Estado']=R
                    print('.............VEHICULO RESERVADO....................')
                    print(lista[i][key])
                    print('.................................')
        break
    return lista            
                  
      
def validardom():  
    dom=0
    while True:      
        try:
            dom1=input('Ingrese el Dominio entre 6 y 9 caracteres sin espacio: ').upper()
            for c in dom1:
                if c.isalpha():
                    dom+=1
            if dom>5 & dom<10:
                break
            else:
                print('El Dominio debe contener entre 6(seis) y 9(nueve) caracter')
                print('..........................................................') 
                              
        except ValueError:
            print('Vuelva a intentar!')
    return dom1       
def contdig(dom):
    letras=0
    for c in dom:
        if c.isalpha():
            letras+=1
    return letras        
def Buscar(lista):
    while True:
        try:        
            amr=input('ingrese el Dominio: ').upper()
            for j in range(0,len(lista)):          
                if amr in lista[j][j]['Dominio1']:
                    print('--------------------------------------------')
                    print("Dominio: ", lista[j][j]['Dominio1']) 
                    print("Marca: ", lista[j][j]['Marca'])  
                    print("Tipo: ", lista[j][j]['Tipo'])  
                    print("Modelo: ", lista[j][j]['Modelo'])  
                    print("Kilometraje: ", lista[j][j]['Kilometraje'])  
                    print("Precio evaluado: $", lista[j][j]['eval'])  
                    print("Precio para la venta: $", lista[j][j]['preciov'])  
                    print("Estado: ", lista[j][j]['Estado']) 
                    print('---------------------------------------------')
                    break
                else:
                 print('Vehiculo no identificado...')
            break
        except ValueError:
            print('ingrese correctamnete')      
    input('Precione enter para continuar...')            

def Ordenar(lista):
    for i in range(len(lista)):
        for j in sorted(lista[i],key=str.lower):
            lista=sorted(lista[j], key=itemgetter(0))
            print('.......................................')
            print(lista)
    return lista      
       
            
def Ordenardis(lista):
    for i in range(len(lista)):
        for j in sorted(lista[i],key=str.lower):
            if lista[i][j]['preciov']>0:
                lista=sorted(lista[i], key=itemgetter(1))
                print('............................................')
                print(lista)
    return lista      






opc=0
lista=[]
while opc!=6:
    opc=int(menu())
    if opc==1:
        lista=Agregar()
        input('Precione enter para continuar...')
    elif opc==2:
        lista=Reservar(lista)
        input('Precione enter para continuar...')
    elif opc==3:
        Buscar(lista) 
    elif opc==4:
        lista=Ordenar(lista)
        input('Precione enter para continuar...')

    elif opc==5:
        lista=Ordenardis(lista)    
        input('Precione enter para continuar...')
    elif opc==6:
        print('FIN DEL PROGRAMA')                   




