import os
import csv
import datetime
def menu():
    os.system('cls')
    print("1) Leer fichero")
    print("2) Mostrar lista por carrera")
    print("3) Generar archivo de los datos buscados anteriormente")
    print("4) Resumen general")
    print("5) Buscar alumno por DNI")
    print("6) Salir del PROGRAMA")
    try:
        opc=int(input("OPCION => "))
        if(opc==1 or opc==2 or opc==3 or opc==4 or opc==5 or opc==6):
            return opc
        else:
            print("Opcion fuera de rango")
    except ValueError:
        print("No se puede seleccionar")

def Leer_fichero(archivo):
    contador=0
    with open('C:\\Users\\ernes\\OneDrive\\Documentos\\MP2021\\mptp04\\tp07\\tp07-Estudiantes.csv', 'r') as registro_alumno:
        registro_alumno=registro_alumno.readlines()
        for alumno in registro_alumno:
            registro_alumno=alumno.split(';')
            for x in range(len(registro_alumno)):
                if x==1:
                    DNI=registro_alumno.pop(x)
                    contador+=1
                
            archivo[DNI]=registro_alumno    
        print("----------------------CARGADO EXITOSAMENTE!----------SE REGISTRO =>",contador,"ALUMNOS----------")
    
    return archivo
def Imprimir_carrera(archivo,seleccion): 
    aprobados=0
    desaprobados=0 
    cont=0      
    carrera=int(input("Ingrese la carrera => 1) Ing.INF; 2) Ing.IND; 3) Ing.QUIM; 4) Ing.MIN; 5) Lis.SIST <= ==>> "))
    if carrera==1:
        informe='IINF'
    elif carrera==2:
        informe='IIND'
    elif carrera==3:
        informe='IQCA'
    elif carrera==4:
        informe='IMIN'
    elif carrera==5:
        informe='LSIS'   
    for codigo, atributo in archivo.items():
        if informe==atributo[1]: 
            
            if atributo[2]!='\n':
                cont+=1
                valor=int(atributo[2])
                print("-----------------------------------------------------------------------------------------------")
                print("NOMBRES: ",atributo[0])
                print("DNI: ",codigo)
                print("CARRERA: ",atributo[1])
                print("NOTA: ", valor)                                       
                VALOR=valor
                if VALOR >= 50:
                    aprobados+=1
                    seleccion[codigo]=archivo[codigo]
                elif VALOR < 50:
                    desaprobados+=1
                    seleccion[codigo]=archivo[codigo]               
    if((desaprobados+aprobados)==cont):
        print("--------------------------------------OTRAS CARACTERISTICAS----------------------------------------")
        porcentaje=(aprobados*100)/cont
        porcentaje2=(desaprobados*100)/cont
        print("CANTIDAD DE ALUMNOS APROBADOS: ",aprobados)
        print(f"PORCENTAJE DE ALUMNOS APROBADOS: {porcentaje:.2f} %")
        print("-----------------------------------------------------------------------------------------------")
        print("CANTIDAD DE ALUMNOS DESAPROBADOS: ",desaprobados)
        print(f"PORCENTAJE DE ALUMNOS DESAPROBADOS: {porcentaje2:.2f} %")
        seleccion['PORCENTAJE']=porcentaje
        #seleccion['PORCENTAJE']=porcentaje2
        seleccion['CARRERA']=informe
    return seleccion
def pregunta(seleccion):
    respuesta=input("¿DESEA IMPRIMIR EN UN ARCHIVO LOS DATOS RECIENTEMENTE (S/N)? => : ").title()
    if respuesta=='N':
        continuar()
    elif respuesta=='S':
        archivar(seleccion)
        print("-------------REALIZADO CON EXITO------------")
    continuar()
def archivar(seleccion):
    #Estudiantes-CARRERA-dd-mm-yyyy.csv
    hoy=datetime.datetime.now()
    fecha=hoy.strftime('%d-%m-%Y')
    carrera=seleccion['CARRERA']
    nombre=('Estudiantes-'+carrera+'-'+fecha)
    hoja=nombre+'.csv'
    cantidad=0
    with open(hoja, 'w', newline='') as file:
        leer=csv.writer(file, delimiter=';')
        num=['NOMBRES','DNI','CARRERA','NOTAS']
        leer.writerow(num)
        
        for codigo in seleccion:   
            lista=seleccion[codigo]
            if(seleccion['CARRERA']!=seleccion[codigo] and seleccion['PORCENTAJE']!=seleccion[codigo]):
                val=int(lista[2])
                lis=[lista[0],codigo,lista[1],val]
                leer.writerow(lis)
                cantidad+=1 
        por=['PORCENTAJE DE APROBADOS',seleccion['PORCENTAJE']]
        leer.writerow(por) 
    file.close()
def resumen(archivo):
    ausente=0
    aprobados=0
    desaprobado=0
    porcentaje=0
    porcentaje1=0
    porcentaje2=0
    print('TOTAL DE ALUMNOS REGISTRADOS: ',len(archivo))
    cantidad=len(archivo)
    for codigo in archivo:
        falta=archivo[codigo]
        if falta[2]!='\n':
            nota=int(falta[2])
            if nota>=50:
                aprobados+=1
                
            elif nota<50:
                desaprobado+=1          
        else:
            ausente+=1
    if ((ausente+aprobados+desaprobado)==cantidad):
        porcentaje=(ausente*100)/cantidad
        porcentaje1=(aprobados*100)/cantidad
        porcentaje2=(desaprobado*100)/cantidad
        print(f"CANTIDAD Y PORCENTAJE DE AUSENTES: {ausente} y {porcentaje:.2f} %")
        print(f"CANTIDAD Y PORCENTAJE DE APROBADOS: {aprobados} y {porcentaje1:.2f} %")
        print(f"CANTIDAD Y PORCENTAJE DE DESAPROBADOS: {desaprobado} y {porcentaje2:.2f} %")
    continuar()
def alumno(archivo):
    dni=input("Ingrese el DNI: ")
    if dni in archivo:
        docu=archivo[dni]
        if docu[2]!='\n':
            print("NOTA DEL ALUMNO ES => ", docu[2])
        else:
            print("AUSENTE => SIN NOTA")    
    else:
        print("ALUMNO NO REGISTRADO")
    continuar()

def continuar():
    input("----------ENTER PARA CONTINUAR----------")





opc=0
archivo={}
seleccion={}
while(opc!=6):
    opc=menu()
    if opc==1:
        archivo=Leer_fichero(archivo)
        continuar()
    elif opc==2:
        seleccion=Imprimir_carrera(archivo,seleccion)
        continuar()
    elif opc==3:
        pregunta(seleccion)
    elif opc==4:
        resumen(archivo)
    elif opc==5:
        alumno(archivo)
    elif opc==6:
        print("----------FIN DEL PROGRAMA--------")
        